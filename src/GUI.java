import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel panelZad1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton OKButton;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JButton OKButton1;
    private JTextField textField7;
    private JTextField textField8;
    private JTextField textField9;
    private JButton OKButton2;
    private JTextField textField10;
    private JTextField textField11;
    private JTextField textField12;
    private JTextField textField13;
    private JButton OKButton3;
    private JButton OKButton4;
    private JTextField textField14;
    private JTextField textField15;
    private JTextField textField16;

    public GUI(){
        JFrame frame=new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(400,300);

        frame.add(panel1);

        OKButton.addActionListener(getOKButtonListener());
        OKButton1.addActionListener(getOKButton1Listener());
        OKButton2.addActionListener(getOKButton2Listener());
        OKButton3.addActionListener(getOKButton3Listener());
        OKButton4.addActionListener(getOKButton4Listener());

        frame.setVisible(true);
    }

    private ActionListener getOKButton4Listener(){
        ActionListener actionListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String in=textField15.getText();
                String out=textField16.getText();
                String key=textField14.getText();

                Vigenere vigenere=new Vigenere(in,out,key);

                if(out.length()==0)
                    out=vigenere.getEncode();
                if(in.length()==0)
                    in=vigenere.getDecode();

                try{
                    textField15.setText(in);
                    textField16.setText(out);
                }catch (NullPointerException e){
                    System.err.println("ERR4");
                }

            }
        };
    return actionListener;
    }

    private ActionListener getOKButton3Listener(){
        ActionListener actionListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String in=textField12.getText();
                String out=textField13.getText();
                String k0=textField10.getText();
                String k1=textField11.getText();

                CaesarCipher caesarCipher=new CaesarCipher(in,out,k0,k1);

                if(out.length()==0)
                    out=caesarCipher.getEncode();
                if(in.length()==0)
                    in=caesarCipher.getDecode();

                try{
                    textField12.setText(in);
                    textField13.setText(out);
                }catch (NullPointerException e){
                    System.err.println("ERR3");
                }

            }
        };
        return actionListener;
    }

    private ActionListener getOKButtonListener(){
        ActionListener actionListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String in=textField2.getText();
                String out=textField3.getText();
                int n=Integer.parseInt(textField1.getText());

                RailFence railFence=new RailFence(in,n,out);

                if(out.length()==0)
                    out=railFence.getEncode();
                if(in.length()==0)
                    in=railFence.getDecode();

                try{
                    textField2.setText(in);
                    textField3.setText(out);
                }catch (NullPointerException e){
                    System.err.println("ERR0");
                }
            }
        };
    return actionListener;}

    private ActionListener getOKButton1Listener(){
        ActionListener actionListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String in=textField5.getText();
                String out=textField6.getText();
                String key=textField4.getText();

                PrzestawienieMacierzowe2a przestawienieMacierzowe2a=new PrzestawienieMacierzowe2a(in,out,key);

                if(out.length()==0)
                    out=przestawienieMacierzowe2a.getEncode();
                if(in.length()==0)
                    in=przestawienieMacierzowe2a.getDecode();

                try{
                    textField5.setText(in);
                    textField6.setText(out);
                }catch (NullPointerException e){
                    System.err.println("ERR1");
                }
            }
        };
        return actionListener;
    }

    private ActionListener getOKButton2Listener(){

        ActionListener actionListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String in=textField8.getText();
                String out=textField9.getText();
                String key=textField7.getText();

                PrzestawienieMacierzowe2b przestawienieMacierzowe2b=new PrzestawienieMacierzowe2b(in,out,key);

                if(out.length()==0)
                    out=przestawienieMacierzowe2b.getEncode();
                if(in.length()==0)
                    in=przestawienieMacierzowe2b.getDecode();

                try{
                    textField8.setText(in);
                    textField9.setText(out);
                }catch (NullPointerException e){
                    System.err.println("ERR2");
                }

            }
        };

        return actionListener;
    }
}
