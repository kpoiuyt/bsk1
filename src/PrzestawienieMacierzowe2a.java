import java.util.ArrayList;
import java.util.Scanner;

public class PrzestawienieMacierzowe2a {

    public int[] key;
    private String in;
    private String out;

    public PrzestawienieMacierzowe2a(String in, String out, String key){

        key="-"+key;

        key=key.replaceAll("\\D+"," ");

        Scanner scanner=new Scanner(key);
        ArrayList<Integer> arrayList = new ArrayList<>();

        while(scanner.hasNextInt())
            arrayList.add(scanner.nextInt());

        this.key=new int[arrayList.size()];

        for(int i=0;i<arrayList.size();i++)
            this.key[i]= arrayList.get(i);

        this.in=in;
        this.out=out;



    }

    public String getEncode(){
        encode();
        return out;
    }

    public String getDecode(){
        decode();
        return in;
    }

   private void decode(){
       int rows=out.length()/key.length;

       if(out.length()%key.length!=0)
           rows++;

       char[][] tab=new char[rows][key.length];

       for(int i=0;i<rows;i++)
           for(int j=0;j<key.length;j++)
               tab[i][j]=0;


       int count=0;
       for(int i=0;i<rows;i++)
           for(int j=0;j<key.length;j++){
           if(count==out.length())
               break;
           tab[i][key[j]-1]=out.charAt(count++);
           }

           StringBuilder stringBuilder=new StringBuilder();
       for(int i=0;i<rows;i++)
           for(int j=0;j<key.length;j++){
           if(tab[i][j]!=0)
               stringBuilder.append(tab[i][j]);
           }

       in=stringBuilder.toString();
    }

    private void encode(){
        int rows=in.length()/key.length;

        if(in.length()%key.length!=0)
            rows++;

        char[][] tab=new char[rows][key.length];

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++)
                tab[i][j]=0;

        int count=0;
        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++) {
            if(count==in.length())
                break;
                tab[i][j] = in.charAt(count++);
            }

        StringBuilder stringBuilder=new StringBuilder();

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++){
            if(tab[i][key[j]-1]!=0)
                stringBuilder.append(tab[i][key[j]-1]);
            }

        out=stringBuilder.toString();
    }
}