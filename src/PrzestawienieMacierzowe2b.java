public class PrzestawienieMacierzowe2b {

    private String in;
    private String out;
    private int[] key;

    public PrzestawienieMacierzowe2b(String in,String out, String key){
        char[] tabChar=new char[key.length()];
        int[] tabInt=new int[key.length()];

        for(int i=0;i<key.length();i++){
            tabChar[i]=key.charAt(i);
            tabInt[i]=0;
        }

        int ascii=65;
        int count=0;


        while(ascii<90){
            for(int i=0;i<tabChar.length;i++)
                if(((int)tabChar[i])==ascii)
                    tabInt[i]=++count;
            ascii++;
        }

        this.key=tabInt;
        this.in=in;
        this.out=out;



    }

    public String getEncode(){
        encode();
        return out;
    }

    public String getDecode(){
        decode();
        return in;
    }

    private void decode(){
        int rows=out.length()/key.length;
        if(out.length()%key.length!=0)
            rows++;

        char[][] tab=new char[rows][key.length];

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++)
                tab[i][j]=0;

        int count=0;

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++){
                if(count==out.length())
                    break;
                tab[i][key[j]-1]=1;
                count++;
            }



        count=0;
        for(int i=0;i<key.length;i++)
            for(int j=0;j<rows;j++)
            {if(count==out.length())
                break;

                if(tab[j][i]!=0)
                    tab[j][i]=out.charAt(count++);
            }


         StringBuilder stringBuilder=new StringBuilder();

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++){
            if(tab[i][key[j]-1]!=0)
                stringBuilder.append(tab[i][key[j]-1]);
            }

            in=stringBuilder.toString();
    }

    private void encode(){
        int rows=in.length()/key.length;
        if(in.length()%key.length!=0)
            rows++;

        char[][] tab=new char[rows][key.length];

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++)
                tab[i][j]=0;

        int count=0;

        for(int i=0;i<rows;i++)
            for(int j=0;j<key.length;j++){
            if(count==in.length())
                break;
            tab[i][key[j]-1]=in.charAt(count++);
            }

            StringBuilder stringBuilder=new StringBuilder();

        for(int i=0;i<key.length;i++)
            for(int j=0;j<rows;j++){
            if(tab[j][i]!=0)
                stringBuilder.append(tab[j][i]);
            }
        out=stringBuilder.toString();

    }




}
