public class CaesarCipher {

    private String in;
    private String out;
    private int n;
    private int k0;
    private int k1;

    public CaesarCipher(String in,String out,String k1,String k2){
        this.in=in;
        this.out=out;
        this.n=26;
        this.k0=Integer.parseInt(k1);
        this.k1=Integer.parseInt(k2);

    }

    public String getEncode(){
        encode();
        return out;
    }

    public String getDecode(){
        decode();
        return in;
    }

    public void decode(){
        int[] tab=new int[out.length()];



        for(int i=0;i<out.length();i++){
            tab[i]=out.charAt(i);
            tab[i]-=65;
            tab[i]=((tab[i]+(n-k0))*k1^11)%n;
            tab[i]+=65;

        }

        StringBuilder stringBuilder=new StringBuilder();

        for(int i=0;i<out.length();i++){
            stringBuilder.append((char)tab[i]);
        }

        in=stringBuilder.toString();
    }

    public void encode(){
        int[] tab=new int[in.length()];

        for(int i=0;i<in.length();i++)
        {   tab[i]=in.charAt(i);
            tab[i]=tab[i]-65;
            tab[i]=((tab[i]*k1)+k0)%n;
            tab[i]+=65;
        }






        StringBuilder stringBuilder=new StringBuilder();
        for(int i=0;i<in.length();i++)
            stringBuilder.append((char)tab[i]);

        out=stringBuilder.toString();


    }


}
