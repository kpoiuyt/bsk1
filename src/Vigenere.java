public class Vigenere {
    private String in;
    private  String out;
    private char[][] table;
    private String key;

    public Vigenere(String in,String out,String key){
        this.in=in;
        this.out=out;
        this.table=new char['Z'+1]['Z'+1];
        this.key=key;
        createTable();
    }

    public String getEncode() {
        encode();
        return out;
    }

    public String getDecode(){
        decode();
        return in;
    }

    private void encode(){
        char[] textTab=in.toCharArray();
        char[] keyTable=key.toCharArray();

        StringBuilder stringBuilder=new StringBuilder();
        for(int i=0;i<textTab.length;i++){
            stringBuilder.append(table[textTab[i]][keyTable[i]]);
        }
        out=stringBuilder.toString();
    }

    private void decode(){
        char[] textTab=out.toCharArray();
        char[] keyTable=key.toCharArray();

        StringBuilder stringBuilder=new StringBuilder();

        for(int i=0;i<textTab.length;i++){
            if(textTab[i]!=' '){
                for(int z='A';z<='Z';z++){
                    if(textTab[i]==table[keyTable[i]][z]){
                        stringBuilder.append(table['A'][z]);
                    }
                }
            }
        }
    in=stringBuilder.toString();
    }

    private void createTable(){
        for(char a='A';a<='Z';a++){
            char b=a;
            for(int z='A';z<='Z';z++){
                if(b=='Z'+1){
                    b='A';
                }
                table[a][z]=b;
                b++;
            }
        }
    }
}
