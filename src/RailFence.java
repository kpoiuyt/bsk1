public class RailFence {

    private String input;
    private String output;
    private int n;
    public RailFence(String input, int n,String output){
        this.input=input;
        this.n=n;
        this.output=output;


    }


    public String getEncode(){
        encode();
        return output;
    }


    public String getDecode(){
        decode();

        return input;
    }

    private void decode(){
        char [][]tab=new char[output.length()][n];

        for(int i=0;i<output.length();i++)
            for(int j=0;j<n;j++)
                tab[i][j]='0';

        int[] p=getPositions();

        int count=0;

        for(int i=0;i<output.length();i++)
           {
                tab[i][p[i]] = '1';
            }




        StringBuilder stringBuilder=new StringBuilder();

        for(int j=0;j<n;j++)
        for(int i=0;i<output.length();i++)
            {
            if(tab[i][j]=='1')
                tab[i][j]=output.charAt(count++);
            }


        for(int i=0;i<output.length();i++)
            for(int j=0;j<n;j++)
                if(tab[i][j]!='0')
                stringBuilder.append(tab[i][j]);


        input = stringBuilder.toString();
    }


    private void encode(){
        char[][] tab=new char[input.length()][n];

        for(int i=0;i<input.length();i++)
            for(int j=0;j<n;j++)
                tab[i][j]='0';

        int[] positions=getPositions();



        for(int i=0;i<input.length();i++)
            tab[i][positions[i]]=input.charAt(i);


       StringBuffer stringBuffer=new StringBuffer();

        for(int j=0;j<n;j++)
        for(int i=0;i<input.length();i++)
            {
            if(tab[i][j]!='0')
                stringBuffer.append(tab[i][j]);
            }

        output=stringBuffer.toString();

    }

    private int[] getPositions() {

        int size=0;
        if(input.length()==0)
            size=output.length();
        else
            size=input.length();

        int[] tab=new int[size];

        for(int i=0;i<size;i++)
            tab[i]=-10;

        try {
            for (int i = 0; i < size; i++) {

                if (i < n)
                    tab[i] = i;
                if (i >= n)
                    for (int j = 2; j < 2 * n; j += 2) {
                        tab[i] = tab[i - j];
                        if (j != 2 * n - 2 && i != size - 1)
                            i++;
                    }


            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.err.println(e.toString());
        }
        return tab;
    }


}
